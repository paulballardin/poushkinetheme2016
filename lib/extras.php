<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
* Load Hero Background Images
*/
add_action('load_background_images', __NAMESPACE__ . '\\os_hero_background');

function os_hero_background() {
    if(is_front_page()){
      $images = get_field('hero_backgrounds');
      $count =  count($images);
      $counter = 1;
      if( $images ): 
          $output_small = 'data-image-small="';
          $output_large = 'data-image-large="';
          $output_alt = 'data-image-alt="';
              foreach( $images as $image ):
                if ($counter != $count ) { 
                $output_small .=  $image['sizes']['large'].',';
                $output_large .=  $image['sizes']['background'].',';
                $output_alt .=  $image['alt'].',';
                } else {
                $output_small .=  $image['sizes']['large'];
                $output_large .=  $image['sizes']['background'];
                $output_alt .=  $image['alt'];
                }
                $counter ++;
              endforeach;
            
          $output_small .= '" ';
          $output_large .= '" ';
          $output_alt .= '" ';     
      endif;

      echo $output_small . $output_large . $output_alt;
    }
}


// Remove Taxonomy From Taxonomy title 

function my_theme_archive_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    }
  
    return $title;
}
 
add_filter( 'get_the_archive_title', __NAMESPACE__ . '\\my_theme_archive_title' );


//Show taxonomy parent menu item
function add_current_nav_class($classes, $item) {

    // Getting the current post details
    global $post;

    // Get post ID, if nothing found set to NULL
    $id = ( isset( $post->ID ) ? get_the_ID() : NULL );

    // Checking if post ID exist...
    if (isset( $id )){

        // Getting the post type of the current post
        $current_post_type = get_post_type_object(get_post_type($post->ID));

        // Getting the rewrite slug containing the post type's ancestors
        $ancestor_slug = $current_post_type->rewrite['slug'];

        // Split the slug into an array of ancestors and then slice off the direct parent.
        $ancestors = explode('/',$ancestor_slug);
        $parent = array_pop($ancestors);

        $post_terms = wp_get_object_terms($id, 'talent_class',array("fields" => "names"));
        $post_terms = array_unique($post_terms);
        $terms = ($post_terms);
        $term = array_pop($terms);

        // Getting the URL of the menu item
        $menu_slug = strtolower(trim($item->url));
 
         // If the menu item URL contains the post type's parent
        if (!empty($menu_slug) && !empty($term) && strpos($menu_slug,$term) !== false) {
            $classes[] = 'current-menu-item';
        }

        // // If the menu item URL contains any of the post type's ancestors
        // foreach ( $ancestors as $ancestor ) {
        //     if (strpos($menu_slug,$ancestor) !== false) {
        //         $classes[] = 'current-page-ancestor';
        //     }
        // }
    }
    // Return the corrected set of classes to be added to the menu item
    return $classes;

} add_action('nav_menu_css_class', __NAMESPACE__ . '\\add_current_nav_class', 10, 2 );


function talent_class_description(){
  if (category_description()){
  echo category_description();
  }
}

add_action('header_content', __NAMESPACE__ . '\\talent_class_description' );







