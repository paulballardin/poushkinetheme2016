/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
        

        // Load typekit js

        try{Typekit.load({ async: true });}catch(e){}


        


        
      },
      finalize: function() {

        // JavaScript to be fired on all pages, after page specific JS is fired
        
        // Position the header image absolutely 
        

        var position_the_logo = function()  {
            var logoWidth = $('.header-inner').width();
            var logoHeight = (logoWidth / 365 * 125);
            var logo = $('header.banner a.brand');
            var test = $('header.banner').css('position');
          
            if (test === 'fixed'){
              $(logo).css('top', - logoHeight - 40);
            }else {
              $(logo).css('top', 'inherit');
            }
            $(logo).fadeIn().css('display', 'inline-block');
        };
         position_the_logo();

         // sizing the category grid containers
        var size_the_grid = function(){
          var wrapHeight = $(window).height();
          var wrap = $('.wrap');
          var topPad = $(wrap).css('padding-top').replace("px", "");
          var bottomPad =$(wrap).css('padding-bottom').replace("px", "");
          var grid = $('.grid');
          // check we have a grid on the page
          if (grid.length > 0) {   
            var gridPadding = $('.grid').css('padding-bottom').replace("px", "");
            var gridElements = $('.grid').length;
            var gridPaddingTotal = (Math.ceil(gridElements/2)-1) * gridPadding;
            var gridHeight = (wrapHeight - topPad - bottomPad - gridPaddingTotal)/2 ;
            $(wrap).find('.grid article').css({
               'height' : gridHeight
            });
          }
        };
        //run the function on doc ready.
        size_the_grid();


        //size the portfolio images
        var size_the_portfolio_image = function(){
          var portfolio_thumb_holder = $('.portfolio-thumb-holder');
          if(portfolio_thumb_holder){
            var width = portfolio_thumb_holder.width();
            console.log('width = ' + width);
            portfolio_thumb_holder.find(' a').css('height', width);
          }
        };
        size_the_portfolio_image();

        
            
      //killer resize function
      var doit;
      function resizedw(){
          position_the_logo();
          size_the_grid();
          size_the_portfolio_image();
          console.log('positioned');
      }
      window.onresize = function() {
          clearTimeout(doit);
          doit = setTimeout(function() {
              resizedw();
          }, 100);
      };

        // End killer resize function
        
        
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
          //
          if (Cookies('loader') !== 'set') {
            $('.home #loader-wrapper').show();
           // Cookies.set('loader', 'set', { expires: 7 });
          } 
          Cookies.remove('loader');

          function getSlides() {
            // Get the slide image array
            var image_small = $("body").data("image-small").split(",");
            
            //Remove any empty elements  
            var arr_small = image_small;

            for (var i = 0; i < arr_small.length; i++ ) {
              if (arr_small[i] !== "") { 
                arr_small[i] = { 'src': arr_small[i]};
                
              }
            }

            var image_large = $("body").data("image-large").split(",");
            
            //Remove any empty elements  
            var arr_large = image_large;

            for (var j = 0; j < arr_large.length; j++ ) {
              if (arr_large[j] !== "") { 
                arr_large[j] = { 'src': arr_large[j]};
                
              }
            }

            return $(window).width() > 768 ? arr_large : arr_small;
        }

        $('body').vegas({
            'delay': 7000,
            'animation': 'kenburns',
            'transition­Duration': 5000, 
             'slides': getSlides()
        });

        $(window).on('resize',function () {
            $('body').vegas('options', 'slides', getSlides());
        });

        var loader = $('#loader').width();
          $('#loader').css('height', loader );

         // JS for the splash page         
          setTimeout(function(){
            $('body').addClass('loaded');
          }, 3000);



              
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },
    // Talent single page, .
    'single_talent': {
      init: function() {
          // JavaScript to be fired on the about us page
          function custom_talent_menu(){ 
          var talent_title = $('#talents-tabs').data('title');
          

          var html = ''; 
          html += '<ul class="nav nav-tabs-nostyle hidden-sm-down" role="tablist" >';
          html += '<li class="nav-link menu-item talent">';
          html += '<h1 id="talent-name"class="">'+ talent_title+'</h1>';
          html +=  '</li>';
          html += '<li class="menu-item  nav-item">';
          html += '<a class="nav-link  active"  data-toggle="tab" href="#gallery-tab" role="tab"><i class="fa fa-minus " aria-hidden="true"></i><i class="fa fa-plus " aria-hidden="true"></i> Portfolio</a>';
          html +=  '</li>';
          html += '<li class="menu-item nav-item" >';
          html +=  '<a class="nav-link" data-toggle="tab" href="#bio-tab" role="tab"><i class="fa fa-minus" aria-hidden="true"></i><i class="fa fa-plus" aria-hidden="true"></i> Bio </a>';
          html += '</li>';
          html += '</ul>';

          var html2 = ''; 
          html2 += '<ul class="nav nav-tabs-nostyle" role="tablist" >';
          html2 += '<li class="nav-link menu-item talent">';
          html2 += '<h1 id="talent-name"class="">'+ talent_title+'</h1>';
          html2 +=  '</li>';
          html2 += '<li class="menu-item  nav-item">';
          html2 += '<a class="nav-link  active"  data-toggle="tab" href="#gallery-tab" role="tab"><i class="fa fa-minus " aria-hidden="true"></i><i class="fa fa-plus " aria-hidden="true"></i> Portfolio</a>';
          html2 +=  '</li>';
          html2 += '<li class="menu-item nav-item" >';
          html2 +=  '<a class="nav-link" data-toggle="tab" href="#bio-tab" role="tab"><i class="fa fa-minus" aria-hidden="true"></i><i class="fa fa-plus" aria-hidden="true"></i> Bio </a>';
          html2 += '</li>';
          html2 += '</ul>';
           $('li.current-talent-parent').after(html);
          $('#talent-meta').html(html2);
         }

         custom_talent_menu();

              



      },
      finalize: function() {
        // JavaScript to be fired on the single-talent page, after the init JS
        $('#talent-tabs a[href="#gallery-tab"]').tab('show'); // Select first tab
        $('#talent-tabs aa[href="#bio-tab"]').tab('show'); // Select last tab

        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox({
              'alwaysShowClose': true,
              'leftArrow ' : '<i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i>',
              'rightArrow ' : '<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>',
              'loadingMessage': 'loading...'
            });

        });




      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
