

<div id="talent-meta" class="w-100 hidden-md-up">Name</div>
<div id="talents-tabs" data-title="<?php the_title(); ?>" class="tab-content">
	<div class="tab-pane fade in active" id="gallery-tab" role="tabpanel" >
	<?php get_template_part('templates/content-single-gallery', get_post_type()); ?>
	</div>
	<div class="tab-pane fade" id="bio-tab" role="tabpanel">
		<?php get_template_part('templates/content-single-talent', get_post_type()); ?>
	</div>
</div>

