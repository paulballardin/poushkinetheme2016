
<?php 
	$thumb = '';
	if (has_post_thumbnail()): 
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
	$thumb = $thumb[0];
	if (!empty($thumb)) { 
		$style =  'style="background-image: url('.$thumb.');"';
	} else {
		$style = '';
	}
	endif;
	$profession = get_field('profession');
	// vars
	$queried_object = get_queried_object(); 

	// load thumbnail for this taxonomy term (term object)
	$singleTax = get_field('singular_talent_label', $queried_object);
	if ($profession === '') {
		$profession = $singleTax; 
		if ($singleTax === ''){
			$profession = the_archive_title();
		}
	} 


?>

<a href="<?php the_permalink(); ?>" title="<?php echo get_the_title() . ' ~ ' . $profession; ?>"> 
	<article <?php post_class(); ?> <?php echo $style; ?> >
		<div class="overlay">
			<div class="overlay-text">
	        	<h2> <?php the_title(); ?></h2> 
	        	<h3> <?php echo $profession; ?></h3>
	        	<p> <?php the_excerpt(); ?></p>
	        </div>      
      </div> 
	  <header>
	    <h2 class="entry-title"><?php the_title(); ?></h2>
	    <h3 class="talent-role"><?php echo $profession; ?></h3>
	  </header>
	</article>
</a/>

