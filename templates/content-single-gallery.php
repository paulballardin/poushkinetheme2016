
<?php 
	$thumb = '';
	if (has_post_thumbnail()): 
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
	$thumb = $thumb[0];
	if (!empty($thumb)) { 
		$style =  'style="background-image: url('.$thumb.');"';
	} else {
		$style = '';
	}
	endif;
	$profession = get_field('profession');
	// vars
	$queried_object = get_queried_object(); 

	// load thumbnail for this taxonomy term (term object)
	$singleTax = get_field('singular_talent_label', $queried_object);
	if ($profession === '') {
		$profession = $singleTax; 
		if ($singleTax === ''){
			$profession = the_archive_title();
		}
	} 


?>

<?php 

$images = get_field('portfolio');

if( $images ): ?>
    <div class="row">
        <?php foreach( $images as $image ): ?>
            <div class="portfolio-thumb-holder col-xs-4 col-sm-4">
                <a class="img-fluid" href="<?php echo $image['url']; ?>" title="<?php echo $image['alt']; ?>" data-toggle="lightbox" data-gallery="example-gallery" style="background-image: url(<?php echo $image['sizes']['portfolio']; ?>);">
                </a>
                <p><?php echo $image['caption']; ?></p>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

