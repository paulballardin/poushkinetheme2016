<header class="banner col-xs-12 col-md-4 col-lg-3">
  <div class="header-inner">
    <nav class="navbar navbar-dark bg-inverse">
      <button class="navbar-toggler hidden-md-up" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    &#9776;
  </button>
      <a class="brand" href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>">
        <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo.png" alt="<?php bloginfo('name'); ?>">
      </a>
  
    <div class="collapse navbar-toggleable-sm" id="navbarCollapse">
      <div class="nav-primary">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
        endif;
        ?>
      </div>
     
      <div class="nav-secondary">
        <?php
        if (has_nav_menu('secondary_navigation')) :
          wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'nav']);
        endif;
        ?>
      </div>
      </div>
      </nav>
  </div>
</header>
